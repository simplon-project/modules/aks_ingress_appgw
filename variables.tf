variable "aks_name" {
  type        = string
  description = "Nom du cluster AKS"
}
variable "subscription_id" {
  type        = string
  description = "ID de l'abonnement Azure"
}
###### add 04.07.24
variable "default_node_pool_name" {
  type        = string
  description = "Nom du pool de nœuds par défaut"
  default     = "default"
}
variable "sku_tier" {
  type        = string
  default     = "Standard"
  description = "The SKU Tier that should be used for this Kubernetes Cluster. Possible values are `Free`, `Standard` and `Premium`"

  validation {
    condition     = contains(["Free", "Standard", "Premium"], var.sku_tier)
    error_message = "The SKU Tier must be either `Free`, `Standard` or `Premium`. `Paid` is no longer supported since AzureRM provider v3.51.0."
  }
}
variable "run_command_enabled" {
  type        = bool
  default     = true
  description = "(Optional) Whether to enable run command for the cluster or not."
}

variable "automatic_channel_upgrade" {
  type        = string
  default     = null
  description = "(Optional) The upgrade channel for this Kubernetes Cluster. Possible values are `patch`, `rapid`, `node-image` and `stable`. By default automatic-upgrades are turned off. Note that you cannot specify the patch version using `kubernetes_version` or `orchestrator_version` when using the `patch` upgrade channel. See [the documentation](https://learn.microsoft.com/en-us/azure/aks/auto-upgrade-cluster) for more information"

  validation {
    condition = var.automatic_channel_upgrade == null ? true : contains([
      "patch", "stable", "rapid", "node-image"
    ], var.automatic_channel_upgrade)
    error_message = "`automatic_channel_upgrade`'s possible values are `patch`, `stable`, `rapid` or `node-image`."
  }
}
variable "local_account_disabled" {
  type        = bool
  default     = null
  description = "(Optional) - If `true` local accounts will be disabled. Defaults to `false`. See [the documentation](https://docs.microsoft.com/azure/aks/managed-aad#disable-local-accounts) for more information."
}

variable "node_os_channel_upgrade" {
  type        = string
  default     = null
  description = " (Optional) The upgrade channel for this Kubernetes Cluster Nodes' OS Image. Possible values are `Unmanaged`, `SecurityPatch`, `NodeImage` and `None`."
}
variable "node_resource_group" {
  type        = string
  default     = null
  description = "The auto-generated Resource Group which contains the resources for this Managed Kubernetes Cluster. Changing this forces a new resource to be created."
}
variable "oidc_issuer_enabled" {
  type        = bool
  default     = false
  description = "Enable or Disable the OIDC issuer URL. Defaults to false."
}

variable "role_based_access_control_enabled" {
  type        = bool
  default     = false
  description = "Enable Role Based Access Control."
  nullable    = false
}

variable "support_plan" {
  type        = string
  default     = "KubernetesOfficial"
  description = "The support plan which should be used for this Kubernetes Cluster. Possible values are `KubernetesOfficial` and `AKSLongTermSupport`."

  validation {
    condition     = contains(["KubernetesOfficial", "AKSLongTermSupport"], var.support_plan)
    error_message = "The support plan must be either `KubernetesOfficial` or `AKSLongTermSupport`."
  }
}
variable "orchestrator_version" {
  type        = string
  default     = null
  description = "Specify which Kubernetes release to use for the orchestration layer. The default used is the latest Kubernetes version available in the region"
}
####variable network_profile

variable "network_plugin" {
  type        = string
  default     = "kubenet" # azure
  description = "Network plugin to use for networking."
  nullable    = false
}
variable "net_profile_dns_service_ip" {
  type        = string
  default     = null
  description = "(Optional) IP address within the Kubernetes service address range that will be used by cluster service discovery (kube-dns). Changing this forces a new resource to be created."
}
variable "ebpf_data_plane" {
  type        = string
  default     = null
  description = "(Optional) Specifies the eBPF data plane used for building the Kubernetes network. Possible value is `cilium`. Changing this forces a new resource to be created."
}
variable "load_balancer_sku" {
  type        = string
  default     = "standard"
  description = "(Optional) Specifies the SKU of the Load Balancer used for this Kubernetes Cluster. Possible values are `basic` and `standard`. Defaults to `standard`. Changing this forces a new kubernetes cluster to be created."

  validation {
    condition     = contains(["basic", "standard"], var.load_balancer_sku)
    error_message = "Possible values are `basic` and `standard`"
  }
}
variable "network_plugin_mode" {
  type        = string
  default     = null
  description = "(Optional) Specifies the network plugin mode used for building the Kubernetes network. Possible value is `overlay`. Changing this forces a new resource to be created."
}

variable "network_policy" {
  type        = string
  default     = null
  description = " (Optional) Sets up network policy to be used with Azure CNI. Network policy allows us to control the traffic flow between pods. Currently supported values are calico and azure. Changing this forces a new resource to be created."
}
variable "net_profile_outbound_type" {
  type        = string
  default     = "loadBalancer"
  description = "(Optional) The outbound (egress) routing method which should be used for this Kubernetes Cluster. Possible values are loadBalancer and userDefinedRouting. Defaults to loadBalancer."
}
variable "net_profile_pod_cidr" {
  type        = string
  default     = null
  description = " (Optional) The CIDR to use for pod IP addresses. This field can only be set when network_plugin is set to kubenet or network_plugin is set to azure and network_plugin_mode is set to overlay. Changing this forces a new resource to be created."
}
variable "net_profile_service_cidr" {
  type        = string
  default     = null
  description = "(Optional) The Network Range used by the Kubernetes service. Changing this forces a new resource to be created."
}
# configuration de service api_server
variable "api_server_authorized_ip_ranges" {
  type        = set(string)
  default     = null
  description = "(Optional) The IP ranges to allow for incoming traffic to the server nodes."
}

variable "api_server_subnet_id" {
  type        = string
  default     = null
  description = "(Optional) The ID of the Subnet where the API server endpoint is delegated to."
}
################################
# variable "node_pool_subnet_id" {
#   type        = string
#   description = "ID du sous-réseau dans lequel le cluster AKS sera déployé"
# }

variable "max_pods" {
  type        = number
  description = "Nombre maximal de pods par nœud dans le cluster AKS"
}

variable "disk_size_gb" {
  type        = number
  description = "Taille des disques pour les nœuds du cluster AKS (en Go)"
}

variable "min_node_count" {
  type        = number
  description = "Nombre minimal de nœuds dans le cluster AKS"
}

variable "max_node_count" {
  type        = number
  description = "Nombre maximal de nœuds dans le cluster AKS"
}

variable "vm_size" {
  type        = string
  description = "Taille des machines virtuelles pour les nœuds du cluster AKS"
}

variable "enable_auto_scaling" {
  type        = bool
  default     = false
  description = "Enable node pool autoscaling"
}

variable "dns_prefix" {
  type        = string
  description = "Préfixe DNS pour le cluster AKS"
}

# variable "vnet_id" {
#   type        = string
#   description = "ID du vnet"
# }

variable "kubernetes_version" {
  description = "Version de Kubernetes à utiliser pour le cluster AKS."
  type        = string
  default     = "1.29.2"
}

variable "kube_config_path" {
  description = "Chemin où le KubeConfig généré sera enregistré."
  type        = string
  default     = null
}

variable "ssh_admin_username" {
  description = "Le nom d'utilisateur pour l'accès SSH aux nœuds."
  type        = string
  default     = "azureuser"
}

variable "gw_name" {
  description = "Le nom de la passerelle d'application"
  type        = string
}

variable "location" {
  description = "L'emplacement de la passerelle d'application"
  type        = string
}

variable "resource_group_name" {
  description = "Le nom du groupe de ressources"
  type        = string
}

variable "domain_name_label" {
  description = "Le label du nom de domaine de l'IP publique"
  type        = string
}

variable "allocation_method" {
  description = "La méthode d'allocation pour l'IP publique"
  type        = string
  default     = "Static"
}

variable "sku" {
  description = "Le SKU de l'IP publique"
  type        = string
  default     = "Standard"
}

variable "sku_name" {
  description = "Le nom du SKU pour la passerelle d'application"
  type        = string
  default     = "Standard_v2"
}

variable "sku_tier_appgw" {
  description = "Le niveau du SKU pour la passerelle d'application"
  type        = string
  default     = "Standard_v2"
}

variable "sku_capacity" {
  description = "La capacité du SKU pour la passerelle d'application"
  type        = number
  default     = 2
}

# variable "application_gateway_subnet_id" {
#   description = "L'ID du sous-réseau"
#   type        = string
# }

variable "frontend_port_name" {
  description = "Le nom du port frontal"
  type        = string
}

variable "frontend_port" {
  description = "Le port du port frontal"
  type        = number
  default     = 80
}

variable "frontend_ip_configuration_name" {
  description = "Le nom de la configuration IP frontale"
  type        = string
  default     = "appGatewayFrontendIP"
}

variable "backend_address_pool_name" {
  description = "Le nom du pool d'adresses de backend"
  type        = string
}

variable "backend_http_settings_name" {
  description = "Le nom des paramètres HTTP de backend"
  type        = string
}

variable "cookie_based_affinity" {
  description = "L'affinité basée sur les cookies"
  type        = string
  default     = "Disabled"
}

variable "backend_http_settings_port" {
  description = "Le port des paramètres HTTP de backend"
  type        = number
  default     = 80
}

variable "backend_http_settings_protocol" {
  description = "Le protocole des paramètres HTTP de backend"
  type        = string
  default     = "Http"
}

variable "request_timeout" {
  description = "Le délai d'attente de la requête des paramètres HTTP de backend"
  type        = number
  default     = 1
}

variable "probe_name" {
  description = "Le nom de la sonde"
  type        = string
}

variable "probe_protocol" {
  description = "Le protocole de la sonde"
  type        = string
  default     = "Http"
}

variable "probe_host" {
  description = "L'hôte de la sonde"
  type        = string
}

variable "probe_path" {
  description = "Le chemin de la sonde"
  type        = string
}

variable "probe_timeout" {
  description = "Le délai d'attente de la sonde"
  type        = number
  default     = 1
}

variable "probe_interval" {
  description = "L'intervalle de la sonde"
  type        = number
  default     = 10
}

variable "unhealthy_threshold" {
  description = "Le seuil malsain de la sonde"
  type        = number
  default     = 3
}

variable "http_listener_name" {
  description = "Le nom de l'écouteur HTTP"
  type        = string
}

variable "http_listener_protocol" {
  description = "Le protocole de l'écouteur HTTP"
  type        = string
  default     = "Http"
}

variable "redirect_configuration_name" {
  description = "Le nom de la configuration de redirection"
  type        = string
}

variable "redirect_type" {
  description = "Le type de la configuration de redirection"
  type        = string
  default     = "Permanent"
}

variable "target_listener_name" {
  description = "Le nom de l'écouteur cible de la configuration de redirection"
  type        = string
  default     = "http-listener"
}

variable "include_path" {
  description = "Le paramètre d'inclusion de chemin de la configuration de redirection"
  type        = bool
  default     = true
}

variable "include_query_string" {
  description = "Le paramètre d'inclusion de chaîne de requête de la configuration de redirection"
  type        = bool
  default     = true
}

variable "request_routing_rule_name" {
  description = "Le nom de la règle de routage de requête"
  type        = string
}

variable "rule_type" {
  description = "Le type de règle de la règle de routage de requête"
  type        = string
  default     = "Basic"
}

variable "log_analytics_workspace_name" {
  description = "Nom du workspace Log Analytics"
  type        = string
}

variable "vnet_name" {
  description = "Le nom du réseau virtuel"
  type        = string
}

variable "subnet_name_for_node_pool" {
  description = "Le nom du sous-réseau pour le pool de nœuds"
  type        = string
}

variable "subnet_name_for_appgw" {
  description = "Le nom du sous-réseau pour la passerelle d'application"
  type        = string
}