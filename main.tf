# Récupération de l'identité du cluster
data "azurerm_resource_group" "main" {
  name = var.resource_group_name
}

# recupération de l'ID du réseau virtuel
data "azurerm_virtual_network" "vnet" {
  name                = var.vnet_name
  resource_group_name = var.resource_group_name
}

# Récupération de l'ID du sous-réseau pour le pool de nœuds
data "azurerm_subnet" "node_pool_subnet" {
  name                 = var.subnet_name_for_node_pool
  virtual_network_name = var.vnet_name
  resource_group_name  = var.resource_group_name
}

# Récupération de l'ID du sous-réseau pour la passerelle d'application
data "azurerm_subnet" "app_gateway_subnet" {
  name                 = var.subnet_name_for_appgw
  virtual_network_name = var.vnet_name
  resource_group_name  = var.resource_group_name
}
resource "tls_private_key" "ssh_key" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

# Création d'un cluster Kubernetes Azure
#tfsec:ignore:azure-container-limit-authorized-ips
#tfsec:ignore:azure-container-use-rbac-permissions
resource "azurerm_kubernetes_cluster" "aks-cluster" {
  name                = var.aks_name
  location            = var.location
  resource_group_name = data.azurerm_resource_group.main.name
  dns_prefix          = var.dns_prefix
  kubernetes_version  = var.kubernetes_version

  sku_tier                          = var.sku_tier
  run_command_enabled               = var.run_command_enabled
  automatic_channel_upgrade         = var.automatic_channel_upgrade
  local_account_disabled            = var.local_account_disabled
  node_os_channel_upgrade           = var.node_os_channel_upgrade
  node_resource_group               = var.node_resource_group
  oidc_issuer_enabled               = var.oidc_issuer_enabled
  role_based_access_control_enabled = var.role_based_access_control_enabled
  support_plan                      = var.support_plan

  # Configuration du profil réseau 
  network_profile {
    network_plugin      = var.network_plugin
    dns_service_ip      = var.net_profile_dns_service_ip
    ebpf_data_plane     = var.ebpf_data_plane
    load_balancer_sku   = var.load_balancer_sku
    network_plugin_mode = var.network_plugin_mode
    network_policy      = var.network_policy
    outbound_type       = var.net_profile_outbound_type
    pod_cidr            = var.net_profile_pod_cidr
    service_cidr        = var.net_profile_service_cidr
  }

  ingress_application_gateway {
    gateway_id = azurerm_application_gateway.appgw.id
  }
  # Configuration du pool de nœuds par défaut
  default_node_pool {
    name                 = var.default_node_pool_name
    vm_size              = var.vm_size
    max_pods             = var.max_pods
    os_disk_size_gb      = var.disk_size_gb
    enable_auto_scaling  = var.enable_auto_scaling
    min_count            = var.min_node_count
    max_count            = var.max_node_count
    vnet_subnet_id       = data.azurerm_subnet.node_pool_subnet.id
    orchestrator_version = var.orchestrator_version
  }
  # Configuration du profil linux
  linux_profile {
    admin_username = var.ssh_admin_username
    ssh_key {
      key_data = tls_private_key.ssh_key.public_key_openssh
    }
  }
  # Configuration de l'agent monitoring
  oms_agent {
    log_analytics_workspace_id      = azurerm_log_analytics_workspace.main.id
    msi_auth_for_monitoring_enabled = true

  }
  # Configuration de l'identité gerer par Azure
  identity {
    type = "SystemAssigned"
  }
  # Configuration du profil de service
  dynamic "api_server_access_profile" {
    for_each = var.api_server_authorized_ip_ranges != null || var.api_server_subnet_id != null ? [
      "api_server_access_profile"
    ] : []

    content {
      authorized_ip_ranges = var.api_server_authorized_ip_ranges
      subnet_id            = var.api_server_subnet_id
    }
  }
  # Configuration du profil de l'auto-scaling
  auto_scaler_profile {
    balance_similar_node_groups      = false
    expander                         = "random"
    max_graceful_termination_sec     = 600
    max_node_provisioning_time       = "15m"
    max_unready_nodes                = 3
    max_unready_percentage           = 45
    new_pod_scale_up_delay           = "10s"
    scale_down_delay_after_add       = "10m"
    scale_down_delay_after_failure   = "3m"
    scan_interval                    = "10s"
    scale_down_unneeded              = "10m"
    scale_down_unready               = "20m"
    scale_down_utilization_threshold = 0.5
    empty_bulk_delete_max            = 10
    skip_nodes_with_local_storage    = true
    skip_nodes_with_system_pods      = true
  }

  # provisioner "local-exec" {
  #   command = "az aks get-credentials --resource-group ${var.resource_group_name} --name ${var.aks_name} --overwrite-existing --file ${var.kube_config_path}"
  # }

  # Configuration du profil de mise à jour
  lifecycle {
    ignore_changes = [default_node_pool[0].node_count]
  }
  depends_on = [azurerm_application_gateway.appgw]
}

# # Kubernetes Config file
# resource "local_file" "kube_config" {
#   count           = var.kube_config_path == null ? 0 : 1
#   content         = azurerm_kubernetes_cluster.aks-cluster.kube_config_raw
#   filename        = var.kube_config_path
#   file_permission = "0644"
# }

# Définition d'une adresse IP publique pour la passerelle d'application
resource "azurerm_public_ip" "gw-ippu" {
  name                = "${var.gw_name}-ippub"
  location            = var.location
  resource_group_name = var.resource_group_name
  domain_name_label   = var.domain_name_label
  allocation_method   = var.allocation_method
  sku                 = var.sku
}

# Définition d'une passerelle d'application Azure
resource "azurerm_application_gateway" "appgw" {
  name                = var.gw_name
  resource_group_name = var.resource_group_name
  location            = var.location

  sku {
    name     = var.sku_name
    tier     = var.sku_tier_appgw
    capacity = var.sku_capacity
  }

  gateway_ip_configuration {
    name      = var.frontend_ip_configuration_name
    subnet_id = data.azurerm_subnet.app_gateway_subnet.id
  }
  frontend_port {
    name = var.frontend_port_name
    port = var.frontend_port
  }

  frontend_ip_configuration {
    name                 = "appGatewayFrontendIP"
    public_ip_address_id = azurerm_public_ip.gw-ippu.id
  }

  backend_address_pool {
    name = var.backend_address_pool_name
  }

  backend_http_settings {
    name                  = var.backend_http_settings_name
    cookie_based_affinity = var.cookie_based_affinity
    port                  = var.backend_http_settings_port
    protocol              = var.backend_http_settings_protocol
    request_timeout       = var.request_timeout
    probe_name            = var.probe_name
  }

  probe {
    name                = var.probe_name
    protocol            = var.probe_protocol
    host                = var.probe_host
    path                = var.probe_path
    timeout             = var.probe_timeout
    interval            = var.probe_interval
    unhealthy_threshold = var.unhealthy_threshold
  }

  http_listener {
    name                           = var.http_listener_name
    frontend_ip_configuration_name = "appGatewayFrontendIP"
    frontend_port_name             = var.frontend_port_name
    protocol                       = var.http_listener_protocol
  }

  redirect_configuration {
    name                 = var.redirect_configuration_name
    redirect_type        = var.redirect_type
    target_listener_name = var.target_listener_name
    include_path         = var.include_path
    include_query_string = var.include_query_string
  }

  request_routing_rule {
    name                       = var.request_routing_rule_name
    rule_type                  = var.rule_type
    http_listener_name         = var.http_listener_name
    backend_address_pool_name  = var.backend_address_pool_name
    backend_http_settings_name = var.backend_http_settings_name
    priority                   = 100
    #redirect_configuration_name = var.redirect_configuration_name
  }

  lifecycle {
    ignore_changes = [
      request_routing_rule,
      http_listener,
      frontend_port,
      backend_http_settings,
      backend_address_pool,
      ssl_certificate,
      probe,
      redirect_configuration,
      url_path_map,
      trusted_root_certificate,
      tags
    ]
  }
}

# Create Network Security Group (NSG)
#tfsec:ignore:azure-network-no-public-ingress
resource "azurerm_network_security_group" "app_gw" {
  name                = "${azurerm_application_gateway.appgw.name}-nsg"
  location            = var.location
  resource_group_name = var.resource_group_name
}

# Associate NSG and Subnet
resource "azurerm_subnet_network_security_group_association" "app_gw" {
  depends_on                = [azurerm_network_security_rule.app_gw_inbound]
  subnet_id                 = data.azurerm_subnet.app_gateway_subnet.id
  network_security_group_id = azurerm_network_security_group.app_gw.id
}

# Create NSG Rules
## Locals Block for Security Rules
locals {
  app_gw_inbound_rules = {
    100 = "80",
    110 = "443",
    130 = "65200-65535" # Azure Application Gateway Health Probe
  }
}
## NSG Inbound Rule for Azure Application Gateway Subnets
#tfsec:ignore:azure-network-no-public-ingress
resource "azurerm_network_security_rule" "app_gw_inbound" {
  for_each                    = local.app_gw_inbound_rules
  name                        = "Rule-Port-${each.value}"
  priority                    = each.key
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "Tcp"
  source_port_range           = "*"
  destination_port_range      = each.value
  source_address_prefix       = "*"
  destination_address_prefix  = "*"
  resource_group_name         = var.resource_group_name
  network_security_group_name = azurerm_network_security_group.app_gw.name
}

resource "azurerm_log_analytics_workspace" "main" {
  name                = var.log_analytics_workspace_name
  location            = var.location
  resource_group_name = var.resource_group_name
  sku                 = "PerGB2018"
  retention_in_days   = 30
}

resource "azurerm_log_analytics_solution" "main" {
  solution_name         = "ContainerInsights"
  location              = azurerm_log_analytics_workspace.main.location
  resource_group_name   = var.resource_group_name
  workspace_resource_id = azurerm_log_analytics_workspace.main.id
  workspace_name        = azurerm_log_analytics_workspace.main.name
  plan {
    publisher = "Microsoft"
    product   = "OMSGallery/ContainerInsights"
  }
}

# Si AGIC est activé
# Création des rôles pour l'identité du cluster
resource "azurerm_role_assignment" "ingress_appgw_identity_vnetcontributor" {
  scope                = data.azurerm_virtual_network.vnet.id
  role_definition_name = "Network Contributor"
  principal_id         = azurerm_kubernetes_cluster.aks-cluster.ingress_application_gateway[0].ingress_application_gateway_identity[0].object_id
}

resource "azurerm_role_assignment" "ingress_appgw_identity_rgreader" {
  scope                = data.azurerm_resource_group.main.id
  role_definition_name = "Reader"
  principal_id         = azurerm_kubernetes_cluster.aks-cluster.ingress_application_gateway[0].ingress_application_gateway_identity[0].object_id
}

resource "azurerm_role_assignment" "ingress_appgw_identity_appgwcontributor" {
  scope                = azurerm_application_gateway.appgw.id
  role_definition_name = "Contributor"
  principal_id         = azurerm_kubernetes_cluster.aks-cluster.ingress_application_gateway[0].ingress_application_gateway_identity[0].object_id
}
