# Sorties pour le cluster AKS
output "aks_cluster_name" {
  value       = azurerm_kubernetes_cluster.aks-cluster.name
  description = "Nom du cluster AKS"
  sensitive   = true
}

output "aks_client_certificate" {
  value       = azurerm_kubernetes_cluster.aks-cluster.kube_config[0].client_certificate
  description = "Certificat client pour le cluster AKS"
  sensitive   = true
}

output "aks_cluster_ca_certificate" {
  value       = azurerm_kubernetes_cluster.aks-cluster.kube_config[0].cluster_ca_certificate
  description = "Certificat CA du cluster AKS"
  sensitive   = true
}

output "aks_kube_config" {
  value       = azurerm_kubernetes_cluster.aks-cluster.kube_config_raw
  description = "Configuration Kubeconfig pour le cluster AKS"
  sensitive   = true
}

output "aks_client_key" {
  value       = azurerm_kubernetes_cluster.aks-cluster.kube_config[0].client_key
  description = "Clé client pour le cluster AKS"
  sensitive   = true
}

output "aks_password" {
  value       = azurerm_kubernetes_cluster.aks-cluster.kube_config[0].password
  description = "Mot de passe pour le cluster AKS"
  sensitive   = true
}

output "aks_username" {
  value       = azurerm_kubernetes_cluster.aks-cluster.kube_config[0].username
  description = "Nom d'utilisateur pour le cluster AKS"
  sensitive   = true
}

output "aks_host" {
  value       = azurerm_kubernetes_cluster.aks-cluster.kube_config[0].host
  description = "Hôte pour le cluster AKS"
  sensitive   = true
}

output "aks_cluster_id" {
  description = "L'ID du cluster Kubernetes."
  value       = azurerm_kubernetes_cluster.aks-cluster.id
  sensitive   = true
}

# Sorties pour la passerelle d'application
output "app_gateway_id" {
  description = "L'ID de la passerelle d'application"
  value       = azurerm_application_gateway.appgw.id
  sensitive   = true
}

output "app_gateway_fqdn" {
  value       = azurerm_public_ip.gw-ippu.fqdn
  description = "Le FQDN de l'IP publique"
}

output "app_gateway_log_analytics_workspace_id" {
  value       = azurerm_log_analytics_workspace.main.id
  description = "L'ID de l'espace de travail Log Analytics"
  sensitive   = true
}

output "app_gateway_name" {
  value       = azurerm_application_gateway.appgw.name
  description = "Le nom de la passerelle d'application"
  sensitive   = true
}
